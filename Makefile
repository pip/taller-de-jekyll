# Si no especificamos la regla, mostramos la ayuda
.DEFAULT_GOAL := help
# Versión de ruby, la podemos pasar como argumento:
# make all ruby_version=2.3.0
ruby_version ?= 2.5.3

# Detecta qué manejador de paquetes estamos usando
is_pacman := $(shell which pacman 2>/dev/null)
is_apt    := $(shell which apt 2>/dev/null)
is_dnf    := $(shell which dnf 2>/dev/null)

# Paquetes a instalar con pacman
ifdef is_pacman
packages := base-devel
install  := sudo pacman -Sy --noconfirm --needed
endif

# Paquetes a instalar con apt
ifdef is_apt
packages := build-essential libssl-dev libreadline-dev zlib1g-dev
install  := sudo DEBIAN_FRONTEND=noninteractive apt install --assume-yes
endif

# Paquetes a instalar con dnf
ifdef is_dnf
packages := @development-tools openssl-devel readline-devel
install  := sudo dnf install
endif

# Agrega rbenv al entorno de la shell
bashrc := ~/.bash_profile ~/.bashrc
$(bashrc): always
	grep -q rbenv $@ || echo 'export PATH="~/.rbenv/bin:$$PATH"' >> $@
	grep -q "rbenv init" $@ || echo 'eval "$$(rbenv init -)"' >> $@

# Instala o actualiza rbenv
~/.rbenv:
	if test -d $@ ; then cd $@ && git pull ; \
	else git clone https://github.com/rbenv/rbenv.git $@ ; fi

# Crea el directorio de plugins
~/.rbenv/plugins:
	mkdir -p $@

# Instala o actualiza el plugin de compilar rubies
~/.rbenv/plugins/ruby-build:
	if test -d $@ ; then cd $@ && git pull ; \
	else git clone https://github.com/rbenv/ruby-build.git $@ ; fi

# Instala la versión de ruby que queremos
~/.rbenv/versions/$(ruby_version):
	~/.rbenv/bin/rbenv install $(notdir $@)
	~/.rbenv/bin/rbenv global $(notdir $@)

# Instala bundler para la versión de ruby
~/.rbenv/versions/$(ruby_version)/bin/bundle:
	~/.rbenv/bin/rbenv exec gem install bundler

# Limpia los binarios para que sean más rápidos y livianos
~/.rbenv/versions/$(ruby_version)/bin/ruby:
	strip --strip-all $@

~/.rbenv/versions/$(ruby_version)/lib/libruby-static.a:
	strip --strip-unneeded $@

# Encuentra todas las librerías de ruby
ruby_lib_dir := ~/.rbenv/versions/$(ruby_version)/lib/ruby
ruby_libs = $(shell test -d $(ruby_lib_dir) && find $(ruby_lib_dir) -type f -name "*.so")
$(ruby_libs): always
	strip --strip-unneeded $@

# Dependencias de rbenv, cada archivo por separado para no entrar en
# infierno de dependencias
rbenv_deps   := ~/.rbenv ~/.rbenv/plugins \
	~/.rbenv/plugins/ruby-build $(bashrc) \
	~/.rbenv/versions/$(ruby_version) \
	~/.rbenv/versions/$(ruby_version)/bin/bundle
# Dependencias de la instalación
install_deps := requisites rbenv
strip_deps := $(ruby_libs) \
	~/.rbenv/versions/$(ruby_version)/lib/libruby-static.a \
	~/.rbenv/versions/$(ruby_version)/bin/ruby

requisites:              ## Instala las dependencias
	$(install) $(packages)
rbenv: $(rbenv_deps)     ## Instala rbenv
install: $(install_deps) ## Instala todo el entorno de trabajo
strip: $(strip_deps)     ## Limpia las librerías
all: install strip       ## Instala todo
upgrade:                 ## Actualiza rbenv
	$(MAKE) -B rbenv
help:                    ## Muestra la ayuda
	@echo "Taller de Jekyll"
	@echo
	@echo "Uso: make [regla] [ruby_version=$(ruby_version)]"
	@echo
	@grep -E "^\w+:.*##.*" $(MAKEFILE_LIST) \
	| sed -re "s/^(\w+):.*##(.*)/\1;\2/" \
	| column -ts ';'

# Esta regla corre siempre aunque el archivo destino exista
always:
.PHONY: always
