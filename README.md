# Taller de Jekyll

## Instalar

Antes de empezar, instalar `git` y `make` usando el manejador de
paquetes de nuestra distribución de preferencia.

```bash
# En Debian, Ubuntu, Mint, etc.
sudo apt install make git
# En Archlinux, Parabola, Manjaro, etc.
sudo pacman -Sy make git
# En Fedora
sudo dnf install make git
```

Luego, clonar este repositorio y dentro de él, correr `make`.

```bash
git clone https://0xacab.org/partido-interdimensional-pirata/taller-de-jekyll.git
cd taller-de-jekyll
make
```
